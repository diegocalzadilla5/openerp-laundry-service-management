##############################################################################
#
# Copyright (c) 2008-2012 NaN Projectes de Programari Lliure, S.L.
#                         http://www.NaN-tic.com
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

{
    "name" : "Laundry Service Management",
    "version" : "0.1",
    "description" : "This module dedicated to Laundry business.",
    "author" : "Ginandjar Satyanagara",
    "website" : "http://www.redpillars.com",
    "depends" : ["base","sale","sale_order_dates","stock","project","account"],
    "category" : "Generic Modules/Laundry",
    "init_xml" : [],
    "demo_xml" : [ 
        
    ],
    "update_xml" : [
        "views/laundry_menu_view.xml",
        "views/laundry_order_view.xml",
        "views/laundry_order_line_label_view.xml",
        "views/laundry_label_view.xml",
        "views/laundry_partner_view.xml",
        "views/laundry_product_view.xml",
    ],
    "active": False,
    "installable": True
}

